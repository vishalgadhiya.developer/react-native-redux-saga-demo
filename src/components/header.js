import React, {useState} from 'react';
import {
  View,
  Button,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {searchProduct, productList} from '../redux/productAction';

const Header = ({title}) => {
  const result = useSelector(state => state.cartDataReducer);
  const [searchFlag, setSearchFlag] = useState(false);

  const dispatch = useDispatch();

  onPressSearch = () => {
    setSearchFlag(true);
  };

  onPressCancelSearch = () => {
    setSearchFlag(false);
    dispatch(productList());
  };

  renderSearchInput = () => {
    return (
      <TextInput
        style={{
          width: '80%',
          height: 35,
          borderColor: 'black',
          borderWidth: 0.5,
          borderRadius: 15,
          paddingHorizontal: 10,
        }}
        placeholderTextColor={'black'}
        placeholder={'Search product...(Ex. Phone, laptop)'}
        onChangeText={text => {
          //console.log('input >>', Text);
          dispatch(searchProduct(text));
        }}></TextInput>
    );
  };
  return (
    <View
      style={{
        width: '100%',
        height: 55,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'pink',
        paddingHorizontal: 20,
      }}>
      {searchFlag ? (
        renderSearchInput()
      ) : (
        <Text style={{fontSize: 18, color: 'black'}}>{title}</Text>
      )}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {searchFlag ? (
          <TouchableOpacity onPress={() => onPressCancelSearch()}>
            <Image
              style={{width: 32, height: 32}}
              source={require('../assets/cancel_black.png')}></Image>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => onPressSearch()}>
            <Image
              style={{width: 28, height: 28}}
              source={require('../assets/search_black.png')}></Image>
          </TouchableOpacity>
        )}

        {true ? (
          <TouchableOpacity  style={{padding:10}} onPress={() => null}>
            <Image
              style={{width: 28, height: 28}}
              source={require('../assets/cart_black.png')}></Image>
            <View style={{position:'absolute', right:0, backgroundColor:'green',borderRadius:45, padding:3, top:0,}}>
                <Text style={{color:'white'}}>{result.length}</Text>
            </View>
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

export default Header;
