/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {addToCart, emptyCart, removeFromCart} from './redux/action';
import {useDispatch, useSelector} from 'react-redux';
import Header from './components/header';
import {productList} from './redux/productAction';

let product = {
  id: 1,
  name: 'Product 1',
  price: 1000,
  color: 'red',
};

const App = () => {
  const dispatch = useDispatch();
  const data = useSelector(state => state.productReducer.data);

  console.log('Data in component  >> ', data);
  useEffect(() => {
    //console.log('Data in component useeffect >> ', data);
    if (data.length <= 0) {
      dispatch(productList());
    }
  }, []);

  addProductToCart = item => {
    dispatch(addToCart(item));
  };

  removeProductFromCart = item => {
    dispatch(removeFromCart(item));
  };

  renderProduct = item => {
    return (
      <View
        key={item.id}
        style={{
          padding: 10,
          borderRadius: 5,
          borderColor: 'grey',
          borderWidth: 1,
          margin: 3,
          flexDirection: 'row',
        }}>
        <Image
          style={{width: 90, height: 90}}
          source={{uri: item.thumbnail}}></Image>
        <View style={{width: '75%'}}>
          <Text style={{marginLeft: 10}}>
            {item.title} ({item.brand})
          </Text>
          <Text style={{paddingHorizontal: 10, height: 50}}>
            {item.description}{' '}
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{marginLeft: 10, color: 'blue', fontSize: 20}}>
              {item.price} $
            </Text>

            <TouchableOpacity
              style={{
                borderColor: 'red',
                borderRadius: 5,
                borderWidth: 0.5,
                justifyContent: 'center',
                padding: 5,
              }}
              onPress={() => removeProductFromCart(item)}>
              <Text style={{fontSize: 10, color: 'red'}}>Remove from Cart</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                borderColor: 'green',
                borderRadius: 5,
                borderWidth: 0.5,
                justifyContent: 'center',
                padding: 5,
              }}
              onPress={() => addProductToCart(item)}>
              <Text style={{fontSize: 10, color: 'green'}}>Add to Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Header title={'Redux + Saga Demo'} noOfCartItem={0} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          
          {/* <Button
            onPress={() => dispatch(productList())}
            color={'blue'}
            title={'Get Product'}
          /> */}
          <View
            style={{
              justifyContent: 'center',
              padding: 10,
            }}>
            {data.products ? (
              data.products.map(item => {
               // console.log('item', item);
                //return <Text style={{margin: 10}}>{item.brand}</Text>;
                return renderProduct(item);
              })
            ) : (
              <Text>Loading...</Text>
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
