import React from "react";
import App from "./App";
import { Provider } from "react-redux";
import store from "./redux/store";
import { compose } from "redux";

const RootApp = () =>{

    console.warn(store);
    return(
        <Provider store={store}>
        <App />
        </Provider>
    );
}

export default RootApp;