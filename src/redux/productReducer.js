import {PRODUCT_LIST, PRODUCT_LOADING, SET_PRODUCT_LIST} from './constant';

const initialState = {
  data: [],
  loading: false,
  error: null,
}

export const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCT_LOADING:
      return {
        ...state,
        loading: true,
      }
    case SET_PRODUCT_LIST:
        console.log("Product list reducer", action.data);
      return {
        ...state,
        loading:false,
        data: action.data
      };
    default:
      return state;
  }
};
