import {put, takeEvery} from 'redux-saga/effects'
import { PRODUCT_LIST, SEARCH_PRODUCT, SET_PRODUCT_LIST } from './constant';

function* getProducts(){
    let data = yield fetch("https://dummyjson.com/products/");
    data = yield data.json();
    console.log("get product called", data);
    yield put({type: SET_PRODUCT_LIST, data});
}

function* searchProducts(queryData){
    console.log("query >>", queryData);
    let data = yield fetch('https://dummyjson.com/products/search?q='+queryData.query);
    data = yield data.json();
    console.log("search called", data);
    yield put({type: SET_PRODUCT_LIST, data});
}

function* productSaga(){
    yield takeEvery(PRODUCT_LIST, getProducts);
    yield takeEvery(SEARCH_PRODUCT, searchProducts);
}

export default productSaga;