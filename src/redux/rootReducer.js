import { combineReducers } from "redux";
import { cartDataReducer } from "./reducer";
import { productReducer } from "./productReducer";

export default combineReducers({
    cartDataReducer,
    productReducer
})