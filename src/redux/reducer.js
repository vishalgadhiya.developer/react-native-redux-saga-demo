import { ADD_TO_CART, REMOVE_FROM_CART,EMPTY_CART } from "./constant";

export const cartDataReducer = (data=[], action) =>{
    console.log("Cart data reducer, action:",action);
    console.log("Cart data reducer, data:",data);

    switch(action.type){
        case ADD_TO_CART:
            console.log("Add to cart call")
        return [action.data, ...data];
        case REMOVE_FROM_CART:
            //data  = data?.slice(1);
            const remainingData = data.filter((item)=> item.id!= action.data.id)
            return [...remainingData];
        case EMPTY_CART:
            data = []
            return [...data];
        default:
            return data;

    }  
}