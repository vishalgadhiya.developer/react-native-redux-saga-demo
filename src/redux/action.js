import { ADD_TO_CART, EMPTY_CART, REMOVE_FROM_CART } from "./constant"


export const addToCart = (data) =>{
    console.log("Call add to cart action")
    return {
        type:ADD_TO_CART,
        data:data
    }
}

export const removeFromCart = (data) =>{
    console.log("Call add to cart action")
    return {
        type:REMOVE_FROM_CART,
        data:data
    }
}

export const emptyCart = () =>{
    console.log("Empty cart action")
    return {
        type:EMPTY_CART,
    }
}

