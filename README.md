# Redux + saga Shoping Cart Demo

### This Shoping Cart demo helps to basic understanding of 
 - redux & saga integration
 - Project structure
 - Call action, send data to store
 - How to call API using middleware(saga) 
 - Show the API result on screen
 - Combine multiple reducer
 - Update the component once data change

### Screenshots:

![01](/screenshots/ss1.png?raw=true "")
![01](/screenshots/ss2.png?raw=true "")

### Learn more about REDUX & SAGA:
https://www.youtube.com/playlist?list=PL8p2I9GklV44lOq17oCf9_f40oXPWvwyS